#!/usr/bin/env python

from distutils.core import setup

setup(name='graphdepth',
      version='1.0',
      description='graph depth and supporting code using kernels',
      author='Daniel Perry',
      author_email='daniel.perry@gmail.com',
      #url='',
      packages=['svdd'],
     )
